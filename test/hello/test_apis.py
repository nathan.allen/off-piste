import logging
from unittest.mock import Mock

import pytest
from gnar_gear import GnarApp

from ..constants import ENVIRONMENT

log = logging.getLogger()


class TestApis:

    @pytest.fixture
    def mock_app(self, monkeypatch):
        monkeypatch.setattr('argparse.ArgumentParser.parse_args',
                            lambda *a, **kw: type('args', (object,), {'port': ''}))
        monkeypatch.setattr('flask.Flask.run', lambda *a, **kw: None)
        monkeypatch.setattr('os.environ', ENVIRONMENT)
        monkeypatch.setattr('postgres.Postgres.__init__', lambda *a, **kw: None)
        mock_sqs_send_message = Mock(return_value={'MessageId': 42})
        mock_client = type('sqs_client', (object,), {'get_queue_url': lambda *a, **kw: {'QueueUrl': 'Canada'},
                                                     'send_message': mock_sqs_send_message})
        monkeypatch.setattr('boto3.Session.client', lambda *a, **kw: mock_client)
        app = GnarApp('off-piste', production=False, port=9400, no_db=True, no_jwt=True)
        app.run()
        yield app.flask.test_client()

    def test_10_45(self, monkeypatch, mock_app):
        response = mock_app.get('/check-in')
        assert response.json == ('Off-Piste checking in! '
                                 'Check back with Piste at /10-45/get-message/42 in 1 minute for a secret message.')
